import React from 'react';
import { Provider } from './store';
import AddSumValue from './components/AddSumValue';
import ValuesList from './components/SumValueList';
import './App.css'

const App = () => {
  return (
    <Provider>
      <div className='App'>
        <AddSumValue/>
        <ValuesList/>
      </div>
    </Provider>
  );
}

export default App;
