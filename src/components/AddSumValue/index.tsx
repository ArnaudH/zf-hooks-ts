import React, { useState } from 'react';
import { useAddSumValue } from '../../hooks/useAddSumValue';
import './style.css';

const AddSumValue = () => {

  console.log("AddSumValue rendered at " + (new Date()).getTime())
  
  const addSumValue = useAddSumValue();

  const [inputValue, setInputValue] = useState('');
  const [isValidValue, setIsValidValue] = useState(false);

  // Handler invoked when the input changes, must update state and check for validity
  const valueChangeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {

    e.preventDefault();

    const val = e.currentTarget.value

    // Update inputValue state
    setInputValue(val)

    // Check if inputvalue is valid, set state accordingly for the error to show
    if (isNaN(val as any) || val === '') {
      setIsValidValue(false)
    } else {
      setIsValidValue(true)
    }

  }

  // Handles the value being added
  const addSumValueHandler = (e: React.MouseEvent<HTMLButtonElement>) => {

    e.preventDefault();

    // validate for input being a number
    if (!isNaN(inputValue as any)) {
      addSumValue(parseInt(inputValue));
    }

  }

  return (
    <div className='Container'>
      <div className='Header-Container'>
        <h4 className="Header">MY SUM</h4>
      </div>
      <div className='Input-Container'>
        <div className='Input-Left'>
          <input 
            className={inputValue === '' || isValidValue ? "Input-Field" : 'Input-Field isInvalid'} 
            onChange={valueChangeHandler}
          />
        </div>
        <div className='Input-Right'>
          <button 
            className="Add-Button" 
            onClick={addSumValueHandler} 
            disabled={!isValidValue}
          >
            ADD
          </button>
      </div>
      <div className='Error-Spacer'>
      {isValidValue || (inputValue === '') ? null : <p className='InvalidInput-Error'>Only numbers are allowed, this isn't algebra.</p>}
      </div>
    </div>
    </div>
  );
}

export default AddSumValue;






// import * as React from 'react';
// import { useState } from 'react';

// import { useFlasher } from '../utils';

// const NewTodo: React.FC = () => {
  
  // return (
    // <li ref={useFlasher()}>
      // <input
        
        // placeholder="Enter title..."
        // onChange={e => }
      // />
      // <button
        // onClick={() => {
          // addTodo(text);
          // setText('');
        // }}
      // >
        // Add
      // </button>
    // </li>
  // );
// };

// export default React.memo(NewTodo);
