import React from 'react';
import SumValue from '../SumValue';
import { useSumValueList } from '../../hooks/useSumValueList';
import { useDeleteSumValues } from '../../hooks/useDeleteSumValues';
import './style.css';


const ValuesList = () => {

  console.log("SumValueList rendered at " + (new Date()).getTime())

  const sumValuesList = useSumValueList();
  const deleteSumValues = useDeleteSumValues();

  return (
    <div className='Container'>
      <div className='Header-Container'>
        <h4 className='Header'>List of values</h4>
      </div>
      <div className='List-Container'>
        <div className='List-Left'>
          {sumValuesList.map(({id, value, isSelected}) => (
            <SumValue key={id} id={id} value={value} isSelected={isSelected} />
          ))}
        </div>
        <div className='List-Right'>
          <button 
            className='Delete-Button'
            onClick={deleteSumValues}
            disabled={!sumValuesList.some(sumValue =>  sumValue.isSelected)}>
              DELETE
          </button>
          <div className='SumValue-Container'>
            <h4 className='Header-Total'>Total value</h4>
            <div className='TotalValue'>
              {sumValuesList.reduce((total, currentSumValue) => total + currentSumValue.value, 0)}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ValuesList;
