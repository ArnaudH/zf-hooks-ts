import React from 'react';
import { memo } from 'react-tracked';
import { useToggleSelectedSumValue } from '../../hooks/useToggleSelectedSumValue';
import './style.css';

const SumValue = ({id, value, isSelected}: SumValueType) => {

  console.log("SumValue " + id + " rendered at " + (new Date()).getTime())

  const toggleSelectedSumValue = useToggleSelectedSumValue();

  // Handles the click event on a sumValue
  const onClickSumValueHandler = (id: number) => {
    toggleSelectedSumValue(id);
  }

  return (
    <div className={isSelected ? 'SumValue Selected' : 'SumValue'} onClick={() => onClickSumValueHandler(id)}>
        {value}
    </div>
  );
};

// Memoize the function to prevent re-render when props have not changed
export default memo(SumValue);
