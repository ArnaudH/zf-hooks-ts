import { useState, useCallback } from 'react';
import { createContainer } from 'react-tracked';
import produce, { Draft } from 'immer';

export type State = {
  sumValuesList: SumValueType[];
};

const initialState: State = {
  sumValuesList: [],
};

const useValue = () => useState(initialState);

const { Provider, useTrackedState, useUpdate: useSetState } = createContainer(
  useValue
);

const useSetDraft = () => {
  const setState = useSetState();
  return useCallback(
    (draftUpdater: (draft: Draft<State>) => void) => {
      setState(produce(draftUpdater));
    },
    [setState]
  );
};

export { Provider, useTrackedState, useSetDraft };
