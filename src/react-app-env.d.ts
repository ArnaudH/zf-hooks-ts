/// <reference types="react-scripts" />

type SumValueType = {
  id: number;
  value: number;
  isSelected: boolean;
};
