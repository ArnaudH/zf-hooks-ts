import { useCallback } from 'react';

import { useSetDraft } from '../store';

// useToggleSelectedSumValue hook
export const useToggleSelectedSumValue = () => {
  const setDraft = useSetDraft();
  return useCallback(
    (id: number) => {
      setDraft(draft => {
        const sumValue = draft.sumValuesList.find(sumValue => sumValue.id === id);
        if (sumValue) sumValue.isSelected = !sumValue.isSelected;
      });
    },
    [setDraft]
  );
};
