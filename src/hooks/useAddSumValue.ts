import { useCallback } from 'react';
import { useSetDraft } from '../store';

let idCounter = 0;

export const useAddSumValue = () => {
  const setDraft = useSetDraft();
  return useCallback(
    (value: number) => {
      setDraft(draft => {
        draft.sumValuesList.push({ id: idCounter++, value, isSelected: false });
      });
    },
    [setDraft]
  );
};
