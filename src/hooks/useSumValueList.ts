import { useTrackedState } from '../store';

export const useSumValueList = () => {
  const state = useTrackedState();
  return state.sumValuesList;
};