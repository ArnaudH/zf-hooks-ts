import { useCallback } from 'react';

import { useSetDraft } from '../store';

export const useDeleteSumValues = () => {
  const setDraft = useSetDraft();
  return useCallback(
    () => {
      setDraft(draft => {
        draft.sumValuesList = draft.sumValuesList.filter(sumValue => !sumValue.isSelected)
      });
    },
    [setDraft]
  );
};
